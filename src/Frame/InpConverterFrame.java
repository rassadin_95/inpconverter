package Frame;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Frame.Actions.GenerateInpAction;
import Frame.Actions.OpenFileAction;

/**
 * Класс окна
 * 
 * @author oleg
 * @date 13.09.2018
 *
 */
public class InpConverterFrame extends JFrame {
	
	/** Файл геометрии(.dat) */
	private File fileDat;
	
	/** Файл напряжения(.rpt) */
	private File fileRpt;
	
	/** Файл с выходными данными(.inp) */
	private File fileInp;
	
	/** Текстовые поля для отображения путей файлов */
	private JTextField fileDatTextField;
	private JTextField fileRptTextField;
	private JTextField fileInpTextField;
	
	
	/**
	 * Конструктор класса
	 * @param title		Заголовок окна
	 */
	public InpConverterFrame(String title) {
		super(title);
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		//Файл .dat
		JLabel label = new JLabel("Файлы с входными данными");
		gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(5, 5, 5, 5);
        panel.add(label, gbc);
        
        JButton openDatButton = new JButton("Открыть");
        openDatButton.addActionListener(new OpenFileAction(this, "dat"));
        gbc.gridwidth = 1;
        gbc.gridy = 1;
        panel.add(openDatButton, gbc);
        
        label = new JLabel("Геометрия:");
        gbc.gridx = 1;
        panel.add(label, gbc);
        
        this.fileDatTextField = new JTextField(35);
        this.fileDatTextField.setEditable(false);
        gbc.gridx = 2;
        gbc.gridwidth = 2;
        panel.add(this.fileDatTextField, gbc);
        
        //Файл .rpt
        JButton openRptButton = new JButton("Открыть");
        openRptButton.addActionListener(new OpenFileAction(this, "rpt"));
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        panel.add(openRptButton, gbc);
        
        label = new JLabel("Напряжения:");
        gbc.gridx = 1;
        panel.add(label, gbc);
        
        this.fileRptTextField = new JTextField(35);
        this.fileRptTextField.setEditable(false);
        gbc.gridx = 2;
        gbc.gridwidth = 2;
        panel.add(this.fileRptTextField, gbc);
        
        label = new JLabel(" ");
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        panel.add(label, gbc);
        
        //Файл с выходными данными
        label = new JLabel("Файл с выходными данными");
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        panel.add(label, gbc);
        
        JButton openInpButton = new JButton("Открыть");
        openInpButton.addActionListener(new OpenFileAction(this, "inp"));
        gbc.gridy = 5;
        gbc.gridwidth = 1;
        panel.add(openInpButton, gbc);
        
        this.fileInpTextField = new JTextField(45);
        this.fileInpTextField.setEditable(false);
        gbc.gridx = 1;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        panel.add(this.fileInpTextField, gbc);
        
        //Кнопки запуска генерации файла и закрытия
        JButton generateButton = new JButton("Преобразовать");
        generateButton.addActionListener(new GenerateInpAction(this));
        gbc.anchor = GridBagConstraints.EAST;
        gbc.gridwidth = 1;
        gbc.gridy = 6;
        gbc.gridx = 2;
        panel.add(generateButton, gbc);
        
        JButton closeButton = new JButton("Закрыть");
        closeButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
        });
        gbc.gridx = 3;
        panel.add(closeButton, gbc);
		
		this.getContentPane().add(panel);
		this.pack();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	/**
	 * @param file
	 */
	public void setFileDat(File file) {
		this.fileDat = file;
		if (this.fileDat != null) {
			this.fileDatTextField.setText(this.fileDat.getPath());
		}
	}
	
	/**
	 * @return fileDat
	 */
	public File getFileDat() {
		return this.fileDat;
	}
	
	/**
	 * @param file
	 */
	public void setFileRpt(File file) {
		this.fileRpt = file;
		if (this.fileRpt != null) {
			this.fileRptTextField.setText(this.fileRpt.getPath());
		}
	}
	
	/**
	 * @return fileRpt
	 */
	public File getFileRpt() {
		return this.fileRpt;
	}
	
	/**
	 * @param file
	 */
	public void setFileInp(File file) {
		this.fileInp = file;
		if (this.fileInp != null) {
			this.fileInpTextField.setText(this.fileInp.getPath());
		}
	}
	
	/**
	 * @return fileInp
	 */
	public File getFileInp() {
		return this.fileInp;
	}
	
	/**
	 * Метод установки файла согласно расширению
	 * @param extension
	 * @param file
	 */
	public void setFileExtension(String extension, File file) {
		if (extension.equals("dat")) {
			this.setFileDat(file);
		} else if (extension.equals("rpt")) {
			this.setFileRpt(file);
		} else {
			this.setFileInp(file);
		}
	}
}
