package Frame.Actions;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Класс фильтра файла по расширению
 * 
 * @author oleg
 * @date 15.09.2018
 *
 */
public class InpConverterFileFilter extends FileFilter {

	/** Допустимое расширение файла */
	private String extension;
	
	
	/**
	 * Конструктор класса
	 * @param extension	Допустимое расширение файлов
	 */
	public InpConverterFileFilter(String extension) {
		this.extension = extension;
	}
	
	/**
	 * Метод получения расширения файла
	 * @param file
	 * @return Расширение файла
	 */
    public static String getExtension(File file) {
        String ext = null;
        String name = file.getName();
        int i = name.lastIndexOf('.');
        if (i > 0 &&  i < name.length() - 1) {
            ext = name.substring(i + 1).toLowerCase();
        }
        return ext;
    }

	@Override
	public boolean accept(File file) {
		if (file.isDirectory()) {
	        return true;
	    }

	    String extension = this.getExtension(file);
	    if (extension != null) {
	        if (extension.equals(this.extension)) {
	                return true;
	        } else {
	            return false;
	        }
	    }
	    
		return false;
	}

	@Override
	public String getDescription() {
		return null;
	}
}
