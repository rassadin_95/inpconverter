package Frame.Actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JOptionPane;

import Frame.InpConverterFrame;

/**
 * Класс действия генерации файла inp
 * 
 * @author oleg
 * @date 15.09.2018
 *
 */
public class GenerateInpAction implements ActionListener {

	/** Ссылка на главное окно приложения */
	private InpConverterFrame frame;
	
	
	/**
	 * Конструктор класса
	 * @param frame
	 */
	public GenerateInpAction(InpConverterFrame frame) {
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		File inpFile = frame.getFileInp();
		int result = JOptionPane.YES_OPTION;
		//Определение inp-файла
		if (inpFile == null) {
			File currentDir = new File(".");
			inpFile = new File(currentDir, "file.inp");
			result = JOptionPane.showConfirmDialog(
                    frame, 
                    "Вы не выбрали файл с выходными данными."
                    		+ "\nРезультат будет записан в\n"
                    		+ inpFile.getPath(),
                    "Предупреждение",
                    JOptionPane.YES_NO_OPTION);
		}
		//Проверка существования dat и rpt файлов
		if (!frame.getFileDat().exists()) {
			JOptionPane.showMessageDialog(frame, 
					"Файл " + frame.getFileDat().getPath() + " не существует",
					"Ошибка",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		if (!frame.getFileRpt().exists()) {
			JOptionPane.showMessageDialog(frame, 
					"Файл " + frame.getFileRpt().getPath() + " не существует",
					"Ошибка",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		if (result == JOptionPane.YES_OPTION) {
			Converter converter = new Converter(
					frame.getFileDat(), 
					frame.getFileRpt(), 
					inpFile);
			frame.setFileInp(converter.runConvertation());
			JOptionPane.showMessageDialog(frame, 
					"Данные записаны в файл\n"
							+ frame.getFileInp().getPath(),
					"Выполнено",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}
}
