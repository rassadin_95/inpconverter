package Frame.Actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import Frame.InpConverterFrame;

/**
 * Класс действия открытия файла геометрии(.dat)
 * 
 * @author oleg
 * @date 15.09.2018
 *
 */
public class OpenFileAction implements ActionListener {

	/** Ссылка на главное окно приложения */
	private InpConverterFrame frame;
	
	/** Допустимое расширение файла */
	private String extension;
	
	
	/**
	 * Конструктор класса
	 * @param frame
	 * @param extension
	 */
	public OpenFileAction(InpConverterFrame frame, String extension) {
		this.frame = frame;
		this.extension = extension;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		JFileChooser fileopen = new JFileChooser();
		fileopen.setCurrentDirectory(new File("."));
		fileopen.setAcceptAllFileFilterUsed(false);
		fileopen.addChoosableFileFilter(new InpConverterFileFilter(this.extension));
		int ret = fileopen.showDialog(null, "Открыть");
		if (ret == JFileChooser.APPROVE_OPTION) {
			File file = fileopen.getSelectedFile();
			
			if (InpConverterFileFilter.getExtension(file).equals(this.extension)) {
				this.frame.setFileExtension(this.extension, file);
			} else {
				JOptionPane.showMessageDialog(frame, 
						"Файл " + file.getPath() +
							"\nне соответствует требуемому формату(" + this.extension + ")",
						"Ошибка", 
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

}
