package Frame.Actions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

/**
 * Класс конвертер dat + rnp = inp
 * 
 * @author oleg
 * @date 15.08.2018
 *
 */
public class Converter {
	
	/** Файл .dat */
	private File datFile;
	
	/** Файл .rnp */
	private File rptFile;
	
	/** Файл .inp */
	private File inpFile;
	
	
	/**
	 * Конструктор класса
	 * @param datFile
	 * @param rntFile
	 * @param inpFile
	 */
	public Converter(File datFile, File rptFile, File inpFile) {
		this.datFile = datFile;
		this.rptFile = rptFile;
		this.inpFile = inpFile;
	}
	
	/**
	 * Метод запуска процесса конвертации
	 */
	public File runConvertation() {
		ArrayList<String> nodeArray = this.readCoordinatesArray();
		ArrayList<String> elementArray = this.readConnectivityArray();
		ArrayList<String> conditionArray = this.readArrayFromRtp();
		
		return this.generateInp(nodeArray, elementArray, conditionArray);
	}
	
	/**
	 * Метод чтения массива COORDINATES из файла dat
	 * @return Список строк, содержащих значения масива
	 */
	public ArrayList<String> readCoordinatesArray() {
		ArrayList<String> result = new ArrayList<String>();
		Scanner scanner;
		
		try {
			scanner = new Scanner(this.datFile);
			
			//Проходим построчно весь файл
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				
				//После нахождения вхождения подстроки "COORDINATES"
				if (line.contains("COORDINATES")) {
					
					//Считываем ещё одну строку из файла
					line = scanner.nextLine();
					
					//Обрабатываем строки из файла пока не достигнем
					//конца массива или файла
					while(scanner.hasNextLine() && 
							!(line = scanner.nextLine()).contains("$")) {
						String[] lineArray = line.split(",");
						result.add(String.format(
								"%8d,%25s,%25s,%25s", 
								Integer.parseInt(lineArray[0]), 
								this.formatNode(Double.parseDouble(lineArray[1]) * 1000), 
								this.formatNode(Double.parseDouble(lineArray[2]) * 1000), 
								this.formatNode(Double.parseDouble(lineArray[3]) * 1000)));
					}
				}
			}
			
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Метод форматирования чисел с плавающей точкой для массива NODE
	 * @param val
	 * @return
	 */
	public String formatNode(Double val) {
		String str = val.toString();
		if (!str.contains("E")) {
			while (str.lastIndexOf("0") + 1 == str.length()) {
				str = str.substring(0, str.length() - 1);
			}
		}
		return str;
	}
	
	/**
	 * Метод чтения массива CONNECTIVITY из файла dat
	 * @return Список строк, содержащих значения масива
	 */
	public ArrayList<String> readConnectivityArray() {
		ArrayList<String> result = new ArrayList<String>();
		Scanner scanner;
		
		try {
			scanner = new Scanner(this.datFile);
			
			//Проходим построчно весь файл
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				
				//После нахождения вхождения подстроки "CONNECTIVITY"
				if (line.contains("CONNECTIVITY")) {
					
					//Считываем ещё одну строку из файла
					line = scanner.nextLine();
					
					//Обрабатываем строки из файла пока не достигнем
					//конца массива или файла
					while(scanner.hasNextLine() && 
							(line = scanner.nextLine()).contains(",")) {
						//Вырезаем "," на конце строки
						if (line.lastIndexOf(",") + 1 == line.length()) {
							line = line.substring(0, line.length() - 1);
						}
						
						//Выделяем числа из строки
						String[] lineArray = line.split(",");
						int[] valArray = new int[lineArray.length];
						for (int i = 0; i < valArray.length; i++) {
							valArray[i] = Integer.parseInt(lineArray[i].trim());
						}
						//Формируем строку массива
						String val = String.format("%8d", valArray[0]);
						for (int i = 2; i < valArray.length; i++) {
							val = val + String.format(",%9d", valArray[i]);
						}
						result.add(val);
					}
					
					break;
				}
			}
			
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Метод чтения массива из файла rtp
	 * @return Список строк, содержащих значения масива
	 */
	public ArrayList<String> readArrayFromRtp() {
		ArrayList<String> result = new ArrayList<String>();
		Scanner scanner;
		
		try {
			scanner = new Scanner(this.rptFile);
			
			//Проходим построчно весь файл
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				
				//После нахождения вхождения подстроки "Component"
				if (line.contains("Component")) {
					//Обрабатываем строки из файла пока не достигнем
					//конца массива или файла
					while(scanner.hasNextLine() && 
							(line = scanner.nextLine()).contains(".")) {
						String[] lineArray = line.trim().replaceAll("[\\s]{2,}", " ").split(" ");
						int intVal = Integer.parseInt(lineArray[0]);
						result.add(String.format(
								"%8d,%25s,%25s,%25s,%25s,%25s,%25s",
								Integer.parseInt(lineArray[0]),
								this.formatRpt(Double.parseDouble(lineArray[1]) / 1000000),
								this.formatRpt(Double.parseDouble(lineArray[2]) / 1000000),
								this.formatRpt(Double.parseDouble(lineArray[3]) / 1000000),
								this.formatRpt(Double.parseDouble(lineArray[4]) / 1000000),
								this.formatRpt(Double.parseDouble(lineArray[5]) / 1000000),
								this.formatRpt(Double.parseDouble(lineArray[6]) / 1000000)));
					}
					
					break;
				}
			}
			
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Метод форматирования чисел с плавающей точкой для массива из файла rtp
	 * @param val
	 * @return
	 */
	public String formatRpt(Double val) {
		String str = val.toString();
		return String.format("%25s", str);
	}
	
	/**
	 * Метод записи в inp-файл
	 * @param node
	 * @param element
	 * @param condition
	 * @return Файл
	 */
	public File generateInp(ArrayList<String> node, ArrayList<String> element, ArrayList<String> condition) {
		//Создание файла в случае отсутствия
		if (!this.inpFile.exists()) {
			try {
				this.inpFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//Запись начального шаблона
		this.writeInp(ConverterTemplate.BEGIN_TEMPLATE, false);
		
		//Запись пути к inp-файлу
		this.writeInp("INPUT_FILE: '" + this.inpFile.getPath() + "'", true);
		
		//Запись шаблона перед массивом NODE
		this.writeInp(ConverterTemplate.NODE_TEMPLATE, true);
		
		//Запись массива NODE
		this.writeInp(node, true);
		
		//Запись шаблона перед массивом ELEMENT
		this.writeInp(ConverterTemplate.ELEMENT_TEMPLATE, true);
		
		//Запись массива ELEMENT
		this.writeInp(element, true);
		
		//Запись шаблона перед массивом CONDITION
		this.writeInp(ConverterTemplate.CONDITIONS_TEMPLATE, true);
		
		//Запись массива CONDITION
		this.writeInp(condition, true);
		
		//Запись шаблона конца файла
		this.writeInp(ConverterTemplate.END_TEMPLATE, true);
		
		return this.inpFile;
	}
	
	/**
	 * Метод записи строки в inp-файл
	 * @param str	Строка
	 * @param cont	Признак дозаписи файла
	 */
	public void writeInp(String str, boolean cont){
		try(FileWriter writer = new FileWriter(this.inpFile, cont))
        {
            writer.write(str);
            writer.append('\n');
            writer.flush();
        }
        catch(IOException e){
        	e.printStackTrace();
        }
	}
	
	/**
	 * Метод записи массива строк в inp-файл
	 * @param template	Массив строк
	 * @param cont		Признак дозаписи файла
	 */
	public void writeInp(String[] template, boolean cont) {
		try(FileWriter writer = new FileWriter(this.inpFile, cont))
        {
			for (String str: template) {
				writer.write(str);
				writer.append('\n');
			}
            writer.flush();
        }
        catch(IOException e){
        	e.printStackTrace();
        }
	}
	
	/**
	 * Метод записи массива строк в inp-файл
	 * @param template	Массив строк
	 * @param cont		Признак дозаписи файла
	 */
	public void writeInp(ArrayList<String> template, boolean cont) {
		try(FileWriter writer = new FileWriter(this.inpFile, cont))
        {
			for (String str: template) {
				writer.write(str);
				writer.append('\n');
			}
            writer.flush();
        }
        catch(IOException e){
        	e.printStackTrace();
        }
	}
}
